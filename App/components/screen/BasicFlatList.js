import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
  FlatList
} from 'react-native'

import firebase from 'react-native-firebase'
import ItemChat from './items/ItemChat'
// import friendsStore from '../../store/FriendsStore';
// import { observer } from 'mobx-react';
// import { isolateGlobalState } from 'mobx/lib/core/globalstate';

const ImagePicker = require('react-native-image-picker')

// More info on all the options is below in the README...just some common use cases shown here
const options = {
  title: 'Select Avatar',
  storageOptions: {
    skipBackup: true,
    path: 'images'
  }
}

export default class BasicFlatList extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerTitle: (
      <View style={
        {
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('profile', {
              name: navigation.state.params.name,
              imageURL: navigation.state.params.imageURL,
              isOnline: navigation.state.params.isOnline,
              userName: navigation.state.params.username,
              phoneNumber: navigation.state.params.phoneNumber,
              uid: navigation.state.params.uid,
              chatID: navigation.state.params.chatID
            })
          }}
        >
          <View style={{ marginRight: 20 }}>
            <Image
              style={{ width: 40, height: 40, borderRadius: 20 }}
              source={navigation.state.params.imageURL ? { uri: navigation.state.params.imageURL } : require('../../assets/images/icon-user.png')}
            />
            {navigation.state.params.isOnline && <View
              style={{
                width: 12,
                height: 12,
                borderRadius: 6,
                position: 'absolute',
                top: 0,
                right: 0,
                marginRight: 0,
                borderColor: 'white',
                borderWidth: 1,
                backgroundColor: '#76FF03'
              }}
            />}
          </View>
        </TouchableOpacity>
        <Text style={{
          textAlign: 'center',
          fontWeight: 'bold',
          fontSize: 20,
          color: 'black'
        }}
        >{navigation.state.params.name}
        </Text>
      </View>
    ),
    headerStyle: {
      backgroundColor: '#40C4FF'
    },
    headerRight:
      <TouchableOpacity style={{ padding: 10 }}>
        <Image
          resizeMode='contain'
          style={{ width: 20, height: 20 }}
          source={require('../img/icon_more.png')}
        />
      </TouchableOpacity>
  })

  constructor(props) {
    super(props)
    this.state = {
      messageText: '',
      arrayMessage: [],
      senderID: '',
    }
  }
  componentWillMount() {
    firebase.database()
      .ref(`/chats/${this.props.navigation.state.params.chatID}`).child('messages')
      .on('child_added', (listMessages) => {
        this.setState({
          arrayMessage: [...this.state.arrayMessage, listMessages.val()]
        })
      })
    firebase.database()
      .ref(`/users/${this.props.navigation.state.params.uid}`)
      .on('value', (userChat) => {
        this.props.navigation.setParams({
          isOnline: userChat.val() ? userChat.val().isOnline : false
        })
      })
  }

  onSendBtnPressed() {
    if (this.state.messageText) {
      console.log(this.props.navigation.state.params.chatID)
      firebase.database().ref(`/chats/${this.props.navigation.state.params.chatID}`)
        .child('messages')
        .push({
          senderID: firebase.auth().currentUser.uid,
          message: this.state.messageText,
          createAt: Math.floor((new Date()).getTime())
        })
        .then(() => {
          console.log('inserted')
        })
        .catch((error) => {
          console.log(error)
        })
      // console.log(this.state.messageArray.toString() + "")
      this.setState({ messageArray: this.state.messageArray })
      this.setState({ messageText: '' })
    }
  }

  render() {
    // console.log('UID',this.props.navigation.state.params.uid)
    // const uid = this.props.navigation.state.params.uid
    // this.props.navigation.setParams({isOnline: friendsStore.friendsObj[uid].isOnline})

    return (
      <KeyboardAvoidingView style={styles.container} {...Platform.OS == 'ios' ? { behavior: 'padding' } : null} >
        <View style={{ flex: 1, backgroundColor: '#E8F5E9' }}>
          <FlatList
            style={{ padding: 10 }}
            data={this.state.arrayMessage}
            renderItem={({ item }) => (
              <ItemChat
                message={item.message}
                senderID={item.senderID}
                images={item.images}
                createAt={item.createAt}
              />
            )}
          />
        </View>

        <View style={styles.footer}>
          <View>
            <TouchableOpacity
              onPress={
                this.show.bind(this)
              }>
              <Image
                style={{ width: 15, height: 15, marginLeft: 15 }}
                source={require('../img/paperclip.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={{
            flex: 1, alignSelf: 'stretch',
          }}>
            <TextInput
              style={styles.textBox}
              onChangeText={(messageText) => this.setState({ messageText })}
              value={this.state.messageText}
              underlineColorAndroid='transparent'
              placeholder='Write a message...' />
          </View>
          <View>
            <TouchableOpacity style={styles.sendBtn} disabled={this.state.disabled}
              onPress={this.onSendBtnPressed.bind(this)}
            >
              <Image
                style={{ width: 50, height: 50, tintColor: 'blue' }}
                source={require('../img/icon_send.png')}
              />
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView >
    )
  }
  show() {
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response)
      const uri = Platform.OS === 'ios' ? response.uri : response.path

      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        const source = { uri: response.uri }
        this.setState({
          avatarSource: source,
        })
        firebase
          .storage()
          .ref(`messageImages/${response.fileName}`)
          .put(uri)
          .then((result) => {
            console.log('uploaded')
            firebase.database().ref(`/chats/${this.props.navigation.state.params.chatID}`)
              .child('messages')
              .push({
                senderID: firebase.auth().currentUser.uid,
                images: result.downloadURL,
                createAt: Math.floor(Date.now())
              })
              .then(() => {
                console.log('inserted')
              })
              .catch((error) => {
                console.log(error)
              })
          })
          .catch(() => {
            console.log('not uploaded yet')
          })
      }
    })
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1,
    backgroundColor: '#ffff99',
  },
  footer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    height: 50,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textBox: {
    alignSelf: 'stretch',
    borderRadius: 7,
    paddingBottom: 8,
    paddingTop: 8,
    textDecorationLine: 'none'
  },
  sendBtn: {
    padding: 5,
  }
})
