import React, { Component } from 'react'

import {
  View,
  TouchableOpacity,
  FlatList
} from 'react-native'
import { observer } from 'mobx-react'
import friendsStore from '../../store/FriendsStore'
import ItemContact from './items/ItemContact'
import chatHistory from '../../store/ChatHistoryStore'
import firebase from 'react-native-firebase';

@observer
export default class ContactScreen extends Component {
  componentDidMount() {
    chatHistory.loadChatHistory()
    friendsStore.loadFriendsList()
  }
  sortKey = (ids) => {
    const res = ids.sort((a, b) => {
      if (a > b) return 1
      if (a < b) return -1
      return 0
    })
    return res.join('|')
  }
  render() {
    const user = friendsStore.friends
    console.log('Freind,', user)
    const { navigate } = this.props.navigation
    return (
      <View style={{ flex: 1, backgroundColor: 'white', padding: 10 }}>
        <FlatList
          style={{ backgroundColor: 'white' }}
          data={friendsStore.friendsList.slice()}
          renderItem={({ item }) =>
            <TouchableOpacity
              onPress={() => {
                // console.log('ITEM', item.key)
                // const key = 
                // firebase
                //   .database()
                //   .ref(`/history/${item.key}`)
                //   .orderByChild('timestamp')
                //   .on('value', (result) => {
                //     console.log('RESULT', result.childKeys)
                //   })
                console.log('UIDTRI', item)

                let keyy = []
                keyy.push(`${firebase.auth().currentUser.uid}`)
                keyy.push(`${item.key}`)
                keyy = this.sortKey(keyy)
                // console.log('KEYYY', keyy)
                firebase.database().ref(`/chats/`).orderByChild('key').equalTo(keyy).once('value', (snap) => {
                  console.log('SNAP', snap)
                  if (snap.val()) {
                    // console.log('KEY', Object.keys(snap.val())[0])
                    navigate('BasicFlatist', {
                      name: item ? item.name : '',
                      chatID: Object.keys(snap.val())[0],
                      uid: item.user ? item.uid : '',
                      isOnline: item ? item.isOnline : false,
                      imageURL: item.imageURL,
                      username: item ? item.email : '',
                      phoneNumber: item ? item.phoneNumber : ''
                    })
                  } else {
                    firebase.database().ref(`/chats/`).push({ key: keyy })
                  }
                })

              }}
            >
              <ItemContact
                imageURL={item.imageURL}
                name={item.name}
                navigation={this.props.navigation}
                isOnline={item.isOnline}
              />
            </TouchableOpacity>
          }
        />
      </View>
    )
  }
}
