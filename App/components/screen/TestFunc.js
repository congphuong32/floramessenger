import React, { Component } from 'react'
import {
  View,
  Text,
  FlatList,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Alert
} from 'react-native'
import firebase from 'react-native-firebase'

export default class FriendRequest extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      listRequest: [],
      user: {}
    }
  }
  componentDidMount() {
    const query = firebase.database().ref(`friendRequests/${firebase.auth().currentUser.uid}`).orderByChild('status').equalTo(3)
    const userID = firebase.auth().currentUser.uid
    query.on('value', (snapshot) => {
      this.setState({
        listRequest: snapshot.val() ? Object.entries(snapshot.val()) : []
      })
    })
    firebase.database().ref(`users/${userID}`).once('value', (userLogged) => {
      this.setState({
        user: userLogged.val()
      })
    })
  }
  getFriendName = async (friendId) => {
    let friendName = ''
    firebase
      .database()
      .ref(`/users/${friendId}`)
      .once('value', (friend) => {
        friendName = friend.val().name + ''
      })
    return friendName
  }

  sendRequest = () => {
    if (this.state.email == '') {
      Alert.alert('Failure', 'Please enter email!')
    } else {
      // const friendId = firebase.database().ref('/')
      firebase
        .database()
        .ref('users/')
        .orderByChild('email')
        .equalTo(this.state.email)
        .once('value', (snapshot) => {
          snapshot.val() != null ?
            snapshot.forEach((child) => {
              firebase
                .database()
                .ref(`friendRequests/${child.key}`)
                .push({
                  status: 3,
                  uid: firebase.auth().currentUser.uid,
                  name: this.state.user.name
                })
                .then(() => {
                  Alert.alert('Success', 'Request sent!')
                  this.setState({
                    email: ''
                  })
                })
                .catch(() => {
                  Alert.alert('Failure', 'Something went wrong, please try again!')
                })
            })
            : Alert.alert('Failure', 'Please enter right email!')
        })
      console.log('friendid', this.state.friendId)
    }
  }
  acceptFriend = (key) => {
    firebase
      .database()
      .ref(`/friendRequests/`)
      .child(`${firebase.auth().currentUser.uid}/${key}`)
      .child('status')
      .set(1)
  }
  discardFriend = (key) => {
    firebase
      .database()
      .ref(`/friendRequests/`)
      .child(`${firebase.auth().currentUser.uid}/${key}`)
      .child('status')
      .set(2)
  }
  render() {
    const { email } = this.state
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'white' }}>
        <View style={{ height: 35, backgroundColor: 'white', flexDirection: 'row', justifyContent: 'center', margin: 10 }}>
          <TextInput
            style={{ flex: 2 / 3, borderBottomColor: 'grey', borderBottomWidth: 1 }}
            value={email}
            onChangeText={t => this.setState({ email: t })}
            underlineColorAndroid='transparent'
            placeholder='Email'
          />
          <TouchableOpacity
            onPress={() => {
              this.sendRequest()
            }}
            style={style.buttonAdd}
          >
            <Text style={{ color: 'black' }}>Send Request</Text>
          </TouchableOpacity>
        </View>
        <FlatList
          data={this.state.listRequest}
          renderItem={({ item }) =>
            <View style={style.items}>
              <Text style={{ flex: 1, fontSize: 18, color: 'black', marginLeft: 5 }}>
                {item[1].name}
              </Text>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <TouchableOpacity
                  onPress={() => {
                    this.acceptFriend(item[0], item[1].name)
                  }}
                  style={{ backgroundColor: '#40C4FF', marginRight: 5, padding: 8, borderRadius: 5 }}
                >
                  <Text>Accept</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    this.discardFriend(item[0], item[1].name)
                  }}
                  style={{ backgroundColor: 'red', marginRight: 5, padding: 8, borderRadius: 5 }}
                >
                  <Text>Discard</Text>
                </TouchableOpacity>
              </View>
            </View>
          }
        />
      </View >
    )
  }
}

const style = StyleSheet.create({
  items: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    margin: 2
  },
  buttonAdd: {
    flex: 1 / 3,
    backgroundColor: 'blue',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5
  }
})
