import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native'

import firebase from 'react-native-firebase'

class ItemChat extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userID: ''
    }
  }
  componentDidMount() {
    this.setState({
      userID: firebase.auth().currentUser.uid
    })
  }

  render() {
    const rightView = (
      <View style={{
        flex: 1,
        alignItems: 'flex-end',
        borderRadius: 10
      }}
      >
        {this.props.message ? <Text style={style.rightView}>{this.props.message}</Text> : null}
        {this.props.images &&
          <Image
            style={style.imagesStyle}
            source={this.props.images ? { uri: this.props.images } : null}
          />}
        <Text>{new Date(this.props.createAt).getHours()}: {new Date(this.props.createAt).getMinutes()}</Text>
      </View>
    )
    const leftView = (
      <View style={{
        flex: 1,
        alignItems: 'flex-start',
        borderRadius: 10
      }}
      >
        {this.props.message ? <Text style={style.leftView}>{this.props.message}</Text> : null}
        {this.props.images &&
          <Image
            style={style.imagesStyle}
            source={this.props.images ? { uri: this.props.images } : null} />}
        <Text>{new Date(this.props.createAt).getHours()}: {new Date(this.props.createAt).getMinutes()}</Text>

      </View>
    )
    return (
      this.state.userID !== this.props.senderID ? leftView : rightView
    )
  }
}

const style = StyleSheet.create({
  leftView: {
    color: 'black',
    backgroundColor: '#BDBDBD',
    borderRadius: 10,
    fontSize: 16,
    position: 'relative',
    padding: 10,
    textShadowRadius: 5,
    includeFontPadding: false,
    textAlignVertical: 'center'
  },
  rightView: {
    color: 'black',
    backgroundColor: '#2196F3',
    borderColor: 'gray',
    borderRadius: 10,
    fontSize: 16,
    position: 'relative',
    padding: 10,
    textShadowRadius: 5,
    includeFontPadding: false,
    textAlignVertical: 'center'
  },
  imagesStyle: {
    width: 150,
    height: 150,
    marginRight: 10,
    marginBottom: 10
  }
})

export default ItemChat
